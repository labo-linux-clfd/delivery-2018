# Rendu Labo Linux de Clermont-Ferrand 2017-2018

## Contenu

### Conférences

Dans le dossier `talks`, vous trouverez toutes les slides des conférences qui
ont été données cette année, triées par intervenant.

### Projets

#### Dashboard

Vous n'êtes pas sans savoir que sur le campus nous avons un écran à l'entrée,
qui à ce jour ne sert pas à grand-chose. Pourquoi ne pas le réhabiliter et lui
redonner une seconde vie ?

*Dashboard* est un ensemble de petits écrans informatifs sur les alentours,
comme les prochains trams, les événements du BDE, la météo, ou juste de la
trivia, comme des citations pleines de sagesse.

Dans le dossier `dashboard`, vous trouverez le travail effectué pour ce projet.
