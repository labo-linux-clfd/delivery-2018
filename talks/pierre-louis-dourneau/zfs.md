# ZFS

---

## Mass storage

> Using only one disk doesn't bring us very far nowadays...

---

We need the ability to:
 - Have capacity greater than a single disk
 - Have redundancy
 
---

What is used in most cases?

> RAID + LVM + XFS

(not accounting for distributed FS which is a whole another subject)

---

### RAID

> Redundant Array of Independent Disks

RAID-0, RAID-1, RAID-3, RAID-5, RAID-6, RAID-10, RAID-50, ...

---

![raid0](raid0.png)

---

![raid1](raid1.png)

---

![raid3](raid3.png)

---

![raid5](raid5.png)

---

![raid6](raid6.png)

---

![raid10](raid10.png)

---

### LVM

![lvm](lvm.png)

Note:
Take a look at all available commands, there is a lot

---

### XFS

Excellent I/O perfs  
Data/Log/Realtime sections

---

*Now imagine them all together...*

---

> Can we do better and easier ?

---

## ZFS story

---

Originally developed by Sun Microsystems for Solaris — 2001  
Comes OpenSolaris, ZFS becomes CDDL licensed — 2005  
OpenSolaris discontinued, open-source fork illumos founded — 2010  
Official announcement of the OpenZFS project — 2013  

Note:
Solaris, pillars for Unix nowadays  
CDDL incompatible with GPL  
When OpenSolaris discontinued, ZFS dev no longer open-source, but with illumos, took what was remaining of CDDL licensed code and done dev in the open  
There is Solaris' ZFS and OpenZFS, two diffenrent beasts (may have some compat issues)  

---

Want to know more about illumos story?

https://youtu.be/-zRN7XLCRhc

![fork yeah](https://i.ytimg.com/an_webp/-zRN7XLCRhc/mqdefault_6s.webp?du=3000&sqp=CMmXxNUF&rs=AOn4CLAwdQLXyjRn_4KBZ-dF8bzya-uCMA)

---

## ZFS overview

---

The basic unit of storage in ZFS is the pool and from it, we obtain datasets
that can be either mountpoints (a mountable filesystem) or block devices.

---

3 layers

![ZFS layers](./zfs-layers.png)

Note:
SPA - handles organizing the physical disks into storage  
DMU - uses storage provided by SPA by r/w to it transactionally in an atomic manner  
DL - translates op on fs and blockdev provided by pool into op in the DMU

---

Low level storage (vdevs)

 * main vdevs
  - `mirror` (n-way mirrors)
  - `raidz{1..3}` ({1..3}-disk parity)
  - `disk` 
  - `file`
 * special vdevs
  - `spare`
  - `cache` (Adaptive Replacement Cache)
  - `log`
 
These vdevs are organized in a pool by the SPA, and form a tree

---

(example)

---

Then, we get datasets from the pool

(example)

---

Each pool and dataset has properties, native or user-defined

---

## Interesting features

---

### 128-bit filesystem

You can store a 16EiB file in a 16EiB volume!

---

### Data integrity

 * Comitted data is stored in a merkle tree (w/ 256-bit checksums)
 * Four disks labels on disk/file vdevs to prevent wiping from head drops
 * Two stage commits
 * ZIL
 * All metadata is stored twice by default, and is stored at 1/8 of disk apart to limit damage caused by head drops
 * Disk labels contain uberblock history
 * Uberblocks contains a sum of all vdev GUIDs
 * N-way mirroring and up to 3 levels of parity
 
Note:
Uberblock history: which allows rollback of the entire pool to a point in the near past in the vent of a worst case scenario  
vdev GUIDs sums: uberblocks valid only if the sum matches, prevents uberblocks from destroyed old pools from being interpreted as valid uberblocks
n-way m and raidz: 2-disk failures are increasingly common  
`zdb -u` for uberblock display  
`zdb -i` for zil
 
---
 
### `zpool history`

Displays command history

---

### Dataset Compression

`-o compression=on|off|gzip|gzip-N|lz4|lzjb|zle`

Note:
Show compressratio

---

### Copies

`-o copies=N`

Only affects newly written data!

---

### Quotas

```
-o quota=<size>|none
-o refquota=<size>|none
-o userquota@user=<size>|none
-o groupquota@group=<size>|none
```

Note:
ref = no descendant accounted

---

### Reservation

```
-o reservation=<size>|none
-o refreservation=<size>|none
```

---

### User permissions

```
zfs allow
zfs unallow
```

---

### `sharesmb` and `sharenfs`

```
-o sharesmb=on|off|<opts>
-o sharenfs=on|off|<opts>
```

---

### Control of synchronous requests

```
-o sync=standard|always|disabled
```

---

### Snapshots

```
zfs snapshot [-r] [-o property=value]... <filesytem>@<snapname>|<volume>@<snapname>
zfs rollback [-Rfr] <snapshot>
zfs clone [-p] [-o property=value]... <snapshot> <filesystem>|<volume>
```

Note:
snapshots are created extremely quickly and consume only data when the active dataset changes  
clones have parent/child dependency

---

### Backups

`zfs send` a snapshot, fs, volume, incrementally, deduped, and resume from token if interrupted

`zfs receive`

---

### Diffs

`zfs diff [-FHt] <snapshot> <snapshot>|<filesytem>`

---

### Scrubs

`zpool scrub [-s | -p] <pool>`

Note:
scrubing vs resilvering

---

### Deduplication

`-o dedup=on`

Note:
Three types of dedup: file-level, block-level, byte-level  
block-level dedup  
Very resource-intensive op  
Recommends 1.25GB of RAM per 1TB of storage

---

## Is it rock solid?

---

Few FS have the same philosophy  
Btrfs development faltered for some time and has scary data loss bugs and got dumped by Red Hat  
ReFS and APFS... (no checksums)  
Watch out for Stratis!

---

## Sources/Hungry for more?

 * [OpenZFS wiki](http://open-zfs.org/wiki/Main_Page)
 * [zfs Man Page](http://illumos.org/man/1m/zfs)
 * [zpool Man Page](http://illumos.org/man/1m/zpool)
