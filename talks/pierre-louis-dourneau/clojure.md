# Clojure

---

## History

----

* Created in 2007
* Latest version 1.9 (8th December 2017)
* Used by _Walmart_, _Puppet_, _Circle CI_, ...

----

### The creator

__Rich Hickey__

* Worked on scheduling systems
* Yield management
* Election projection systems
* C++/Java/C#

Note: CLisp discovered, wrote scheduling with it, but nobody wanted to run it
in production, rewrite in C++ 4 times as long => something wrong  
Ragequit after 18 years of programming exp  
Created clojure  
Makes awesome talks  

----

> Effective Programs - 10 Years of Clojure - Rich Hickey

youtu.be/2V1FtfBDsLU

---

## What is Clojure ?

----

> A lisp dialect for functional programming symbiotic with an established
> platform designed for concurrency.

---

## Lisp

----

```clj
(+ 1 1)
```

----

```clj
(+ 1 1)  ; => 2
         ╿
         │
         └ A comment
```

----

```clj
(+ (* 2 3) (/ 6 2))
```

----

```clj
(+ (* 2 3) (/ 6 2))

(+    6       3   )

          9
```

----

```clj
value
(operator [operands...])
```

---

And variables? No, symbols.

```clj
;; Global
(def global-1 "Hello")
           ╿
           └ Yes, all unicode characters can be used
             when declaring a symbol!

;; Local
(let [local-example "World"]
  (println global-1 local-example))
```

----

Parens everywhere???

```clj
(let [n 10]
  (reduce
    str
    (take
      n
      (map
        clojure.string/upper-case
        (repeatedly (partial rand-nth
                             "abcdefghijklmnopqrstuvwxyz"))))))
```

----

_And macros..._

---

## Functional Programming

----

_function_

----

### Pure functions

```js
let a = 1;

function b (x) { a += x; }
b(5);  // Impure!

function c (a, x) { return a + x; }
a = c(a, 3);  // Pure! — No side-effects
```

_Transformations, not mutations._

Note: Enable to think about your system in small parts.

----

### Immutability

----

### ~~Immutability~~ State

_Challenges of modeling the world_

----

#### Imperative Programming

* Manipulates its world directly
* World is stopped while you look at or change it

Note: do this/do that, changing mem locations

----

* "_The world is stopped_" is no longer true
* Multiple "omnipotent" participants must somehow avoid destroying others' presumptions
* ~~Solutions: mutexes, locks, ...~~

----

#### Functional Programming

* More mathematical view of the world
* Sees programs as functions taking values and producing others
* Eschew external effects of imperative programs
* Easier to reason about as activity of functions is local

----

* The world may not a function, sadly

----

#### Object-Oriented Programming

* Attempt to provide tools for modeling identity and state
* Unifies both => object (_identity_) is a pointer to the memory that contains
  the value of its _state_

Note: Identity: a stable logical entity associated with a series of different
values over time  
State:  
Associating behaviour with state is ignored here

----

* No way to obtain the state independent of the identity other than copying it
* No way to observe a stable state without blocking others from changing it
* No way to associate the identity's state with a different value other than
  in-place memory mutation

Note: compare with imperative programming => same!

----

#### Clojure programming

* An identity is not a state, an identity __has__ a state.
* Identity is associated to different state values __over time__.

----

More on the value of Values:  
youtu.be/-6BsiVyC1kM

----

### Composability

> "It is better to have 100 functions operate on one data structure than to
> have 10 functions operate on 10 data structures." - Alan J. Perlis

----

Only 4 datastructures

```clj
'(1 2 3 4)    ; lists
 [1 2 3 4]    ; vectors
 {:a 1 :b 2}  ; maps
#{1 2 3 4}    ; hash-sets/sorted-sets
```

----

### Dynamic typing

* Differs from most FP languages (e.g. _Haskell_)

Note: testing still dominates real-world effectiveness  
names dominates semantics

----

Persistent Datastructure — immutable _and_ fast

![](pvector.png)

----

![](pvector-struct-sharing.png)

----

```clj
(def brown [0 1 2 3 4 5 6 7 8])
(def blue  (assoc brow 5 'beef))
```

![](pvector-update.png)

----

### Laziness

```clj
(take 20 (map #(* % 2) (repeatedly #(rand))))
```

---

## Concurrency

----

Typical issues — Immutability gets you 90% there

----

### Built-in

* Futures (run task on another thread, and use it when needed)
* Delays (like futures but do not execute until `deref`ed)
* Promises (define result without the task that produces it)

----

#### `core.async`

* CSP, Golang-style

```clj
(defn reverser [in]
  (let [out (chan)]
    (go (while true (>! out (clojure.string/reverse (<! in)))))
    out))

(defn printer [in]
  (go (while true (println (<! in)))))

(def in-chan (chan))
(def reverser-out (reverser in-chan))
(printer reverset-out)

(>!! in-chan "repaid")  ; => diaper
```

----

#### Dealing with state

* vars (`def`/`set!` — root/per-thread bindings)
* refs (STM — mutation only within transactions)
* agents (like ref — but async, they're reactive)
* atoms (`swap!`)

---

## Macros

----

__Homoiconicity__

```clj
(def a (* 3 4))

(or (= a 10) (> a 12))

;; is rewritten to
(if (= a 10)
  (= a 10)
  (if (> a 12)
    (> a 12)
    nil)
```

---

## "Self-hosted"

* Core implemented on JVM
* Rest written in Clojure
* Other runtimes: JS, CLR, ...

----

### Interop

```clj
(doto (JFrame.)
  (.add (doto (JLabel. "Hello World")
          (.setHorizontalAlignment SwingConstants/CENTER)))
  .pack .show)
```

Can also use clj libs as java libs — "just hand me the jar"

Same for ClojureScript and JS

---

## A high-quality and innovative ecosystem

* clojure.core (600+ fns)
* core.async (CSP)
* core.logic (logic programming)
* core.typed (gradual typing)
* core.spec (specifies structure of data)
* plumbing.graph (graph-based structured computation)

Note: core.spec validates or destructures data and can generate data based on
these spec

---

## Writing clj vs doing clj

---

## Getting started

---

## And more...

* Multimethods
* Protocols
* Records
* Transactions
* Namespaces
* Metadata
* Reducers
* Transients
* nrepl
* Validators
* Watchers
* ...
---

## Recommendations

---

## Thanks !
